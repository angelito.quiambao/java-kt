<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Database</title>
	</head>
	<body>
		<!-- Retrieve the data from the servlet context -->
		<% 
		ServletContext srvContext = getServletContext();
		String firstName = srvContext.getAttribute("firstName").toString();
		String lastName = srvContext.getAttribute("lastName").toString();
		String email = srvContext.getAttribute("email").toString();
		String contact = srvContext.getAttribute("contact").toString();
		%>
		
		<!-- Display the data with JSP Expression -->
		<h1>User Information Stored in Database</h1>
		<p>First Name: <%= firstName %></p>
		<p>Last Name: <%= lastName %></p>
		<p>Email: <%= email %></p>
		<p>Contact: <%= contact %></p>
	</body>
</html>