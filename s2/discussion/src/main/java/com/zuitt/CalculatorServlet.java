package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	// Servlet LifeCycle
	// Initialization is the first stage of the servelet lifecycle.
	// This stage is when the servlet is first initialize upon use:
	// - loads the servlet class
	// - creates an instance of the servlet class
	// - the init() method initializes the servlet instance.
	
	public void init() throws ServletException{
		System.out.println("********************");
		System.out.println("Initialized Servlet.");
		System.out.println("Connected to DB.");
		System.out.println("********************");
	}
	
	// service methods (doPost, service()) - are the second stage of  the servlet lifecycle that handles servlet request and responses.
	//doPost() - for post request
	//service() - for any request
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator");
		
		// To get the data passed form the input to our query string we have to use the getParameter method of our request.
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		
		// system out shortcut: type sysout and then press ctrl + space
		System.out.println("The sum of the two numbers are: ");
		System.out.println(num1+num2);
		
		//getWriter method from the response to produce an output in the page.
		PrintWriter out = res.getWriter();
		
		// Create a total variable:
		int total = num1 + num2;
		
		// print a string in our browser
//		out.println("The total of two numbers are: " + total);
		
		//getWriter can output not only string but also HTML elements
		out.println("<h1>The total of two numbers are: " + total + "</h1>");
	}
	
	//doGet() service methods handles get method request
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You have access the GET service method of the calculator servlet.</h1>");
	}
	
	// finalization - the last stage of the servlet's life cycle:
	// - Cleanup resources once the servlet is destroyed or unused.
	// - Close connections.
	// the destroy() method finalizes the servlet.
	
	public void destroy() {
		System.out.println("********************");
		System.out.println("Finalizing Servlet.");
		System.out.println("Disconnected to DB.");
		System.out.println("********************");
	}
	
	
}
