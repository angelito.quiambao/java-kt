package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException{
		System.out.println("********************");
		System.out.println("Initialized Servlet.");
		System.out.println("Connected to DB.");
		System.out.println("********************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operator = req.getParameter("operation");
		
		int result = 0;
		String operation = "";
		
		PrintWriter out = res.getWriter();
		
		//System.out.println(operator);
		
		switch(operator) {
			case "+":
				result = num1 + num2;
				operation = "add";
				break;
			case "-":
				result = num1 - num2;
				operation = "subtract";
				break;
			case "*":
				result = num1 * num2;
				operation = "multiply";
				break;
			case "/":
				if(num2 < 1) {
					out.println("<h1>Arithmetic Error!</h1>");
				}
				else {
					result = num1 / num2;
				}
				operation = "divide";
				break;
			default:
				out.println("<h1>Invalid operation provided!</h1>");
		}
		
		out.println(
				"<p>The two numbers you provided are: " + num1 + ", " + num2 + "</p>"+
				"<p>The operation that you wanted is: "+ operation + "</p>"+
				"<p>The result is: "+ result +"</p>"
				);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println(
				"<h1>You are now using the calculator app.</h1>" + 
				"<p>To use the app, input two number and an operation.</p>"+
				"<p>Hit the submit button after filling in the details.</p>"+
				"<p>You will get the result shown in your browser!</p>"
				);
	}
	
	public void destroy() {
		System.out.println("********************");
		System.out.println("Finalizing Servlet.");
		System.out.println("Disconnected to DB.");
		System.out.println("********************");
	}

}
