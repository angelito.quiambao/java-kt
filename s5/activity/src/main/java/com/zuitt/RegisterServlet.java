package com.zuitt;

import java.io.IOException;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns="/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	
	public void init() {
		System.out.println("**************************");
		System.out.println("RegisterServlet Initialized");
		System.out.println("**************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		// Capture all form data input
		String firstName = req.getParameter("firstname");
		String lastName = req.getParameter("lastname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String accountType = req.getParameter("account_type");
		String appDiscovery = req.getParameter("app_discovery");
		String dateOfBirth = req.getParameter("date_of_birth");
		String comments = req.getParameter("comments");
		
		HttpSession session = req.getSession();
		session.setAttribute("firstname", firstName);
		session.setAttribute("lastname", lastName);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("accountType", accountType);
		session.setAttribute("appDiscovery", appDiscovery);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("comments", comments);
		
		//redirect to register.jsp
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("*************************");
		System.out.println("RegisterServlet Finalized");
		System.out.println("*************************");
	}
}
