package com.zuitt;

import java.io.IOException;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns="/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8286177122798244808L;
	
	public void init() {
		System.out.println("*********************************");
		System.out.println("LoginServlet has been initialized");
		System.out.println("*********************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		HttpSession session = req.getSession();
		String firstName = session.getAttribute("firstname").toString();
		String lastName = session.getAttribute("lastname").toString();
		String fullName = firstName + " " + lastName;
		
		session.setAttribute("fullname", fullName);
		
		res.sendRedirect("home.jsp");
	}
	
	public void destroy() {
		System.out.println("*********************************");
		System.out.println("LoginServlet has been finalized");
		System.out.println("*********************************");
	}

}
