<%
	String accountType = session.getAttribute("accountType").toString();
	String message = "";
	if(accountType.equals("employer")){
		accountType = "Employer";
		message = "Welcome back employer! Ready to posts job?";
	}
	else if(accountType.equals("applicant")){
		accountType = "Applicant";
		message = "Welcome back applicant! Ready to apply?";
	}
	else{
		message = "Account is not specified!";
	}
	
	session.setAttribute("message", message);
%>