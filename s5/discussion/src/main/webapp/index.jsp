<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>JSP Dynamic Content</title>
	<style type="text/css">
		div{
			margin-top: 5px;
			margin-bottom: 5px;
		}
		
		#container{
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
		}
	</style>
	</head>
	<body>
		<div id="container">
			<h1>JSP Servlet Transport</h1>
			<form action="booking" method="post">
			
				<div>
					<label for="name">Name: </label>
					<input type="text" name="name" id="name" />
				</div>
				
				<div>
					<label for="phone">Phone: </label>
					<input type="text" name="phone" id="phone" />
				</div>
				
				<div>
					<label for="email">Email: </label>
					<input type="email" name="email" id="email" />
				</div>
				<!-- Vehicle choice -->
				<fieldset>
					<legend>Which vehicle do you need?</legend>
					<!-- Taxi -->
					<input type="radio" id="taxi" name="car_type" required value="taxi" />
					<label for="taxi">Taxi</label>
					<!-- Four Seater -->
					<input type="radio" id="four_seater" name="car_type" required value="fourseater" />
					<label for="four_seater">4 Seater Car</label>
					<!-- Six Seater -->
					<input type="radio" id="six_seater" name="car_type" required value="sixseater" />
					<label for="six_seater">6 Seater Car</label>
				</fieldset>
				<!-- extras -->
				<fieldset>
					<legend>Extras</legend>
					
					<input type="checkbox" name="extras_baby" value="baby_seat"/>
					<label for="extras_baby">Baby Seat</label>
					
					<input type="checkbox" name="extras_wheelchair" value="wheelchair_assistance"/>
					<label for="extras_wheelchair">Wheelchair Assistance</label>
				</fieldset>
				<!-- pickup time -->
				<div>
					<label for="pickup_date_time">Pickup Date/Time</label>
					<input type="datetime-local" name="pickup_date_time" required>
				</div>
				<!-- pickup location -->
				<div>
					<label for="pickup_location">Pickup Location</label>
					<select id="pickup_place" name="pickup_location">
						<option value="" selected="selected">Select One</option>
						<option value="office">Office</option>
						<option value="home">Home</option>
					</select>
				</div>
				<!-- dropoff location -->
				<div>
					<label for="destination">Dropoff Place</label>
					<input type="text" name="destination" required list="destinations">
					<datalist id="destinations">
						<option value="Home">
						<option value="Office">
						<option value="Airport">
						<option value="Beach">
					</datalist>
				</div>
				<!-- Comment box -->
				<div>
					<label for="comments">Special Instructions</label>
					<br>
					<textarea name="comments" maxlength="500"></textarea>
				</div>
				
				<!-- Submit -->
				<button>Submit Booking</button>
			</form>
		</div>
	</body>
</html>