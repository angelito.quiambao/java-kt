<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    <%@ include file="carType.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Confirmation Page</title>
	</head>
	<body>
		<%
			/* In JSP, sesison is a pre-defined variable in JSP. This is the HttpSessionObject */
			/* In JSP, out is a predefined variable in JSP. This is the PrintWriter object */
			String pickupDateTime = session.getAttribute("pickupDateTime").toString();
			pickupDateTime = pickupDateTime.replace("T", "/");
		%>
		
		<h1>Booking Confirmation</h1>
		<p>Name: <%= session.getAttribute("name") %></p>
		<p>Phone Number: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>Car Type: <%= carType %></p>
		<p>Extras:</p>
		<ul>
			<%
				 if(session.getAttribute("extrasBaby") == null && session.getAttribute("extrasWheelchair") == null){
					 out.println("<li>No Extras Requested</li>");
				 }
			
				if(session.getAttribute("extrasBaby") != null){
					 out.println("<li>Baby Seat</li>");
				 }
				
				if(session.getAttribute("extrasWheelchair") != null){
					 out.println("<li>Wheelchair Assistance</li>");
				 }
			%>
		</ul>
		<p>Pickup Date and Time <%= pickupDateTime %></p>
		<p>Pickup Location: <%= session.getAttribute("pickupLocation") %></p>
		<p>Destination: <%= session.getAttribute("destination") %></p>
		<p>Comments: <%= session.getAttribute("comments") %></p>
		<br>
		
		<form action="database" method="post">
			<input type="submit">
		</form>
		
		<!-- Link can be used but form button is used instead for visual purposes -->
		
		<form action="index.jsp">
			<input type="submit" value="back">
		</form>
	</body>
</html>