package com.zuitt;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns="/database")
public class DatabaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6668827760365993356L;
	
	ArrayList<String> bookings = new ArrayList<>();
	private int bookingCount;
	
	public void init() {
		System.out.println("*************************************");
		System.out.println("Database servlet has been initialized");
		System.out.println("*************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		//Generate current date and time
		String date = LocalTime.now().toString();
		
		//increase the booking count by one to be used for the order number.
		bookingCount++;
		
		//Generate Custom Booking number with booking count and date
		String orderNumber = bookingCount + date.replaceAll("[^a-zA-z0-9]+", "");
//		System.out.println(date);
		System.out.println(orderNumber);
		
		//Add the Generated Order Number to the Arraylist/Database
		bookings.add(orderNumber);
		
		//Add the bookings to be retrieved in all servlet.
		HttpSession session = req.getSession();
		session.setAttribute("bookings", bookings);
		
		//redirect to booking.jsp
		res.sendRedirect("bookings.jsp");
	}
	
	public void destroy() {
		System.out.println("***********************************");
		System.out.println("Database servlet has been finalized");
		System.out.println("***********************************");
	}
}
