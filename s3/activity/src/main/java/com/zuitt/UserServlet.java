package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	public void init() throws ServletException {
		System.out.println("******************");
		System.out.println("UserServlet has been initialized");
		System.out.println("******************");
	}
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		// System Properties
		String fName = req.getParameter("fname");
		System.getProperties().put("firstName", fName);
		
		//HttpSession
		String lName = req.getParameter("lname");
		HttpSession session = req.getSession();
		session.setAttribute("lastName", lName);
		
		//Servlet Context
		String email = req.getParameter("email");
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email", email);
		
		//Url Rewriting
		String contact = req.getParameter("contact");
		res.sendRedirect("details?contact="+contact);
	}
	
	public void destroy() {
		System.out.println("******************");
		System.out.println("UserServlet has been finalized");
		System.out.println("******************");
	}
}
