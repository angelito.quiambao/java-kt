package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	
	public void init() throws ServletException {
		System.out.println("******************");
		System.out.println("DetailsServlet has been initialized");
		System.out.println("******************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		PrintWriter out = res.getWriter();
		
		//Servlet Context Parameter
		ServletContext srvContext = getServletContext();
		String phonebook = srvContext.getInitParameter("phonebook");
		System.out.println(phonebook);
		
		// System Properties
		String firstName = System.getProperty("firstName");
		System.out.println(firstName);
		
		//HttpSession
		HttpSession session = req.getSession();
		String lastName = session.getAttribute("lastName").toString();
		System.out.println(lastName);
		
		//Servlet Context
		String email = srvContext.getAttribute("email").toString();
		System.out.println(email);
		
		//Url Writing
		String contact = req.getParameter("contact");
		
		out.println(
				"<h1>"+ phonebook +"</h1>" +
				"<p>First Name: " + firstName + "</p>" +
				"<p>Last Name: " + lastName + "</p>" +
				"<p>Contact Number: " + contact + "</p>" +
				"<p>Email: " + email + "</p>"
				);
		
	}
	
	public void destroy() {
		System.out.println("******************");
		System.out.println("DetailsServlet has been finalized");
		System.out.println("******************");
	}
}
