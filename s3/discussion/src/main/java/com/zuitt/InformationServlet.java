package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class InformationServlet extends HttpServlet {

	// Add Database Placeholder
	private ArrayList<String> data;
	
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException {
		//Initialize the servlet with the following data
		data = new ArrayList<>(Arrays.asList("Standard", "Deluxe"));
		System.out.println("******************");
		System.out.println("InformationServlet has been initialized");
		System.out.println("******************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		
		//we use Servlet Context to retrieve the data set in as context params in our web.xml
		// we use .getInitParameter(key/param-name) to retrieve the data.
		
		ServletContext srvContext = getServletContext();
		String hotelName = srvContext.getInitParameter("hotel_name");
		String hotelAddress = srvContext.getInitParameter("hotel_address");
		String hotelContact = srvContext.getInitParameter("hotel_contact");
		
		//retrieve init
		ServletConfig srvConfig = getServletConfig();
		String status = srvConfig.getInitParameter("status");
		
		//retrieve system properties:
		//String facilities = System.getProperty("facilities");
		
		String facilities = req.getParameter("facilities");
		
		//retrieve data from the session:
		HttpSession session = req.getSession();
		// a error will occur because any data pass in the session when retrieved will be generic object.
		String roomsAvailable = session.getAttribute("availableRooms").toString();
		
		out.println(
				"<h1>Hotel Information</h1>" +
				"<p> Hotel Name: " + hotelName + "</p>" +
				"<p> Hotel Address: " + hotelAddress + "</p>" +
				"<p> Hotel Contact: " + hotelContact + "</p>" +
				"<p> Rooms Status: " + status + "</p>" +
				"<p> Hotel Facilities: " + facilities + "</p>"+
				"<p> Rooms Available: " + roomsAvailable + "</p>"
				);
		
		
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String roomName = req.getParameter("roomtype");
		
		data.add(roomName);
		
		PrintWriter out = res.getWriter();
		
		out.println(data);
	}
	
	public void doPut(HttpServletRequest req, HttpServletResponse res) throws IOException {
		data.set(1, "Jane");
		
		PrintWriter out = res.getWriter();
		
		out.println(data);
	}
	
	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws IOException {
		data.remove(1);
		
		PrintWriter out = res.getWriter();
		
		out.println(data);
	}
	
	public void destroy() {
		System.out.println("******************");
		System.out.println("InformationServlet has been finalized");
		System.out.println("******************");
	}
}
